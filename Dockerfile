FROM debian:jessie
ENV DEBIAN_FRONTEND noninteractive

# Variables ####################################################################
ENV nfs_entrypoint "/usr/local/bin/entrypoint"
ENV nfs_add_export "/usr/local/bin/add_export"
ENV export_options "rw,sync,no_subtree_check,crossmnt,fsid=root,no_root_squash"
ENV export_directory "/exports"
ENV udp_port "111"
ENV tcp_port "2049"

# Packages #####################################################################
RUN apt-get update --quiet --quiet \
 && apt-get install --quiet --quiet --assume-yes \
    inotify-tools \
    nfs-kernel-server \
 && rm --recursive --force /var/lib/apt/lists/*

# NFS ##########################################################################
COPY nfs_entrypoint.sh "${nfs_entrypoint}"
COPY nfs_add_export.sh "${nfs_add_export}"
RUN chmod +x "${nfs_entrypoint}"
RUN chmod +x "${nfs_add_export}"

RUN mkdir --parents "${export_directory}"
VOLUME "${export_directory}"

EXPOSE "${udp_port}"/udp "${tcp_port}"/tcp

# This should be the $nfs_entrypoint variable, but we can't use the
# shell-interpreted version of ENTRYPOINT for signal-handling reasons.
ENTRYPOINT ["/usr/local/bin/entrypoint"]
