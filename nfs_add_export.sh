#!/usr/bin/env bash

set -euo pipefail

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <export-1> [ <export-2> [ <export-3> [...] ] ]"
   echo
   echo "Arguments:"
   echo "  export-N: Absolute path to export target"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" == 0 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

exports="${@}"
for x in ${exports[@]}; do
   echo -e "$x\t*($export_options)" >> /etc/exports
done

exportfs -r
