# Docker NFS Server

Use this container to share volumes over NFS.

## Usage

```
docker build --tag=chpatton013/docker-nfs-server .
modprobe nfs
modprobe nfsd
docker run --privileged --rm --name=nfs-server --publish=2049:2049 --publish=111:111 chpatton013/docker-nfs-server
docker exec nfs-server add_export <list of exported directories>
```

In order to maintain an environment in which exported directories can persist,
you must run this container without a command. This will cause the default
command to run, which blocks indefinitely. After the container is running, you
can issue additional commands with `docker exec`.

You can add volumes to this container from the host machine and from other
containers:

* From host machine: `--volume=<host-machine-path>:<container-mount-path>`
* From other containers: `--volumes-from=<container-name>`

It is recommended to use this container's `/exports` directory as the mount
location for host machine volumes. This will allow you to easily forward those
mounted volumes onto other containers with `--volumes-from`.
