#!/usr/bin/env bash

set -euo pipefail

source /etc/default/nfs-kernel-server
source /etc/default/nfs-common

echo -e "$export_directory\t*($export_options)" >> /etc/exports

rpcbind
service nfs-kernel-server start

exec inotifywait --monitor "$export_directory"
